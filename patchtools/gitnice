#!/bin/bash
#
# gitnice
#

declare arg=
declare opt=
declare numregex='^[0-9]+$'
declare number=
declare cmtauth=
declare cmtdate=
declare autdate=
declare reverse=
declare hashwid="%<(8)"
declare authwid=""
declare fmtstr=""
declare b_format=true
declare hashsiz="h"
declare usagestr=

declare gitold=
declare gver=
declare -i gmaj=
declare -i gmin=

gver=$(git --version | cut -d' ' -f3)
gmaj=$(echo "$gver" | cut -d'.' -f1)
gmin=$(echo "$gver" | cut -d'.' -f2)
[ "$gmaj" -lt 2 ] && [ "$gmin" -lt 8 ] && gitold=true || gitold=false


usagestr=$(
cat <<EOF

$(basename "$0") [options] [commitexpr]

  Displays commits in --oneline format, with options as detailed below.
  With no options and no arguments, the most recent commit is displayed
  as "git log --oneline -n1"

  commitexpr - Commit expression can be any valid commit expression, e.g.
  1234567, or 89abcde...f0123456, or -12

  Options
  -c        - show commit date
  -d        - show author date
  -a        - show author
  -s number - space out author to columns of "number" width (git 2.8+ only)
  -r        - show in reverse order (oldest first)
  -w        - show full 40-char hash. Default is "--short"
  -h        - this help text

  Additionally, except for formatting options, any valid git log option can
  also be entered.
\0
EOF
)

usage() {
	echo -e "$usagestr"
	exit "$1"
}

# If output is to terminal, then do the special formatting.
# Else, if being redirected, just plain output.
#
# [ -t 1 ] && b_format=true || b_format=false

test_help() {
	local opt="$1"

	# shopt -s nocasematch
	[[ $opt =~ (h|help) ]] && usage 0
	# shopt -u nocasematch
}

for arg in "$@"; do

	test_help "$1"

	if [ "${arg:0:1}" == '-' ]; then
		opt="${arg:1}"

		if [[ $opt =~ $numregex ]]; then
			number=$opt;
			shift
			continue
		fi

		case "$opt" in
		s ) $gitold && continue
			shift
			authwid="%<($arg,mtrunc)"
			shift
			;;
		c ) $b_format && cmtdate="%C(bold green)%cd " || autdate="%cd "
			shift
			;;
		d ) $b_format && autdate="%C(bold green)%ad " || autdate="%ad "
			shift
			;;
		a ) $b_format && cmtauth="%C(bold yellow)%ae " || cmtauth="%ae "
			shift
			;;
		r ) reverse="--reverse"
			shift
			;;
		w ) hashsiz="H"
			shift
		esac
	fi
done

$gitold && hashwid=
[ "$cmtauth" ] || authwid=""

$b_format && \
	fmtstr="$hashwid%C(bold green)%$hashsiz $cmtdate$autdate$authwid$cmtauth%C(reset)%s" \
|| \
	fmtstr="$hashwid%$hashsiz $cmtdate$autdate$authwid$cmtauth %s"

[ -n "$number" ] && number="-""$number"
if [ -z "$*" ] && [ -z "$number" ]; then
	number="-1"
fi

git log --date=short --pretty=format:"$fmtstr" $reverse $number "$@"

exit $?
